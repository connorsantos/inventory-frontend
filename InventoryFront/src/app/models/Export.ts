import { Warehouse } from './Warehouse';
import { User } from './User';


export class Export{
    id:number;
    warehouse:Warehouse;
    user:User;
    exportDate:Date;
    orderNumber:string;
    comment:string;
    status:string;

    constructor(
        input_id:number,
        input_warehouse:Warehouse,
        input_user:User,
        input_exportDate:Date,
        input_orderNumber:string,
        input_comment:string,
        input_status:string
    ){
        this.id=input_id;
        this.warehouse=input_warehouse;
        this.user=input_user;
        this.exportDate=input_exportDate;
        this.orderNumber=input_orderNumber;
        this.comment=input_comment;
        this.status=input_status;
    }
}