export class Category{
    id:number;
    category:string;
    status:string;

    constructor(
        input_id:number,
        input_category:string,
        input_status:string
    ){
        this.id=input_id;
        this.category=input_category;
        this.status=input_status;
    }
}
