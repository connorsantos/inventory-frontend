
export class Amount{
    id:number;
    amount:string;
    status:string;

    constructor(
        input_id:number,
        input_amount:string,
        input_status:string
    ){
        this.id=input_id;
        this.amount=input_amount;
        this.status=input_status;
    }
}
