

export class State{
    id:number;
    state:string;
    status:string;

    constructor(
        input_id:number,
        input_state:string,
        input_status:string
    ){
        this.id=input_id;
        this.state=input_state;
        this.status=input_status
    }
}