
import { Amount } from './Amount';
import { Category } from './Category';


export class Product{
    id:number;
    amount:Amount;
    category:Category;
    name:string;
    min_amount:number;
    price:number;
    status:string;

    constructor(
        input_id:number,
        input_amount:Amount,
        input_category:Category,
        input_name:string,
        input_min_amount:number,
        input_price:number,
        input_status:string
    ){
        this.id=input_id;
        this.amount=input_amount;
        this.category=input_category;
        this.name=input_name;
        this.min_amount=input_min_amount;
        this.price=input_price;
        this.status=input_status;
    }
}