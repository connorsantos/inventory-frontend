import { City } from './City';

export class Supplier{
    id:number;
    city:City;
    name:string;
    address:string;
    zipcode:string;
    phone:string;
    status:string;

    constructor(
        input_id:number,
        input_city:City,
        input_name:string,
        input_address:string,
        input_zipcode:string,
        input_phone:string,
        input_status:string
    ){
        this.id=input_id;
        this.city=input_city;
        this.name=input_name;
        this.address=input_address;
        this.zipcode=input_zipcode;
        this.phone=input_phone;
        this.status=input_status;
    }
}