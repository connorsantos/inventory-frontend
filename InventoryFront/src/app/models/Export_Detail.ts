import { Product } from './Product';
import { Export } from './Export';



export class User{
    id:number;
    export:Export;
    product:Product;
    quantity:number;
    status:string;

    constructor(
        input_id:number,
        input_export:Export,
        input_product:Product,
        input_quantity:number,
        input_status:string
    ){
        this.id=input_id;
        this.export=input_export;
        this.product=input_product;
        this.quantity=input_quantity;
        this.status=input_status;
    }
}