import { Warehouse } from './Warehouse';
import { Product } from './Product';



export class Import_Detail{
    id:number;
    warehouse:Warehouse;
    product:Product;
    quantity:number;
    status:string;

    constructor(
        input_id:number,
        input_warehouse:Warehouse,
        input_product:Product,
        input_quantity:number,
        input_status:string
    ){
        this.id=input_id;
        this.warehouse=input_warehouse;
        this.product=input_product;
        this.quantity=input_quantity;
        this.status=input_status;
    }
}