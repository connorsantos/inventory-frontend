import { User } from './User';

export class User_detail{
    id:number;
    user:User;
    firstname:string;
    lastname:string;
    email:string;
    status:string;

    constructor(
        input_id:number,
        input_user:User,
        input_firstname:string,
        input_lastname:string,
        input_email:string,
        input_status:string
    ){
        this.id=input_id;
        this.user=input_user;
        this.firstname=input_firstname;
        this.lastname=input_lastname;
        this.email=input_email;
        this.status=input_status
    }
}