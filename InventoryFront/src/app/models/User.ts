import { Role } from './Role';

export class User{
    id:number;
    role:Role;
    username:string;
    password:string;
    attempts:number;
    status:string;

    constructor(
        input_id:number,
        input_role:Role,
        input_username:string,
        input_password:string,
        input_attempts:number,
        input_status:string
    ){
        this.id=input_id;
        this.role=input_role;
        this.username=input_username;
        this.password=input_password;
        this.attempts=input_attempts;
        this.status=input_status;
    }
}