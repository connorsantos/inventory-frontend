import { City } from './City';

export class Company{
    id:number;
    city:City;
    name:string;
    address:string;
    phone:string;
    status:string;
    zipcode:string;


    constructor(
        input_id:number,
        input_city:City,
        input_name:string,
        input_address:string,
        input_phone:string,
        input_status:string,
        input_zipcode:string
    ){
        this.id=input_id;
        this.city=input_city;
        this.name=input_name;
        this.address=input_address;
        this.phone=input_phone;
        this.status=input_status;
        this.zipcode=input_zipcode;
    }
}