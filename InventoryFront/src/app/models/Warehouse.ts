import { City } from './City';
import { Company } from './Company';

export class Warehouse{
    id:number;
    company:Company;
    city:City;
    address:string;
    zipcode:string;
    phone:string;
    status:string;

    constructor(
        input_id:number,
        input_company:Company,
        input_city:City,
        input_address:string,
        input_zipcode:string,
        input_phone:string,
        input_status:string
    ){
        this.id=input_id;
        this.company=input_company;
        this.city=input_city;
        this.address=input_address;
        this.zipcode=input_zipcode;
        this.phone=input_phone;
        this.status=input_status
    }
}