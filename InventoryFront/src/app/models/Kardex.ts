import { Warehouse } from './Warehouse';
import { Product } from './Product';


export class Kardex{
    id:number;
    warehouse:Warehouse;
    product:Product;
    imports:number;
    exports:number;
    instock:number;
    status:string;

    constructor(
        input_id:number,
        input_warehouse:Warehouse,
        input_product:Product,
        input_imports:number,
        input_exports:number,
        input_instock:number,
        input_status:string
    ){
        this.id=input_id;
        this.warehouse=input_warehouse;
        this.product=input_product;
        this.imports=input_imports;
        this.exports=input_exports;
        this.instock=input_instock;
        this.status=input_status;
    }
}