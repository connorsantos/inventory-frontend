import { States } from './States';

export class City{
    id:number;
    state:States;
    city:string;
    status:string;

    constructor(
        input_id:number,
        input_state:States,
        input_city:string,
        input_status:string
    ){
        this.id=input_id;
        this.state=input_state;
        this.city=input_city;
        this.status=input_status
    }
}