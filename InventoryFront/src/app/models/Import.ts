import { Warehouse } from './Warehouse';
import { User } from './User';

export class Import{
    id:number;
    warehouse:Warehouse;
    user:User;
    importDate:Date;
    orderNumber:string;
    comment:string;
    status:string;

    constructor(
        input_id:number,
        input_warehouse:Warehouse,
        input_user:User,
        input_importDate:Date,
        input_orderNumber:string,
        input_status:string
    ){
        this.id=input_id;
        this.warehouse=input_warehouse;
        this.user=input_user;
        this.importDate=input_importDate;
        this.orderNumber=input_orderNumber;
        this.status=input_status;
    }
}