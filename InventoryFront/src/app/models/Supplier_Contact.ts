
import { State } from './State';
import { Supplier } from './Supplier';


export class Supplier_Contact{
    id:number;
    supplier:Supplier;
    firstname:string;
    lastname:string;
    email:string;
    phone:string;
    state:State;

    constructor(
        input_id:number,
        input_supplier:Supplier,
        input_firstname:string,
        input_lastname:string,
        input_email:string,
        input_phone:string,
        input_state:State
    ){
        this.id=input_id;
        this.supplier=input_supplier;
        this.firstname=input_firstname;
        this.lastname=input_lastname;
        this.email=input_email;
        this.phone=input_phone;
        this.state=input_state;
    }
}