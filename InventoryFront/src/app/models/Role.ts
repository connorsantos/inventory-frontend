export class Role{
    id:number;
    role:string;
    status:string;

    constructor(
        input_id:number,
        input_role:string,
        input_status:string
    ){
        this.id=input_id;
        this.role=input_role;
        this.status=input_status
    }
}